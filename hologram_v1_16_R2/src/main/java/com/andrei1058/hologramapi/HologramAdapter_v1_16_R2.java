package com.andrei1058.hologramapi;

import com.mojang.datafixers.DataFixUtils;
import com.mojang.datafixers.types.Type;
import net.minecraft.server.v1_16_R2.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.Map;

public class HologramAdapter_v1_16_R2 implements HologramVersion {

    public void registerPlayer(Player player) {
        if (((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().get("HologramAPI") == null) {
            ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().addBefore("packet_handler", " HologramAPI", new PacketListener_v1_16_R2(player.getUniqueId()));
        }
    }

    public EntityHologramLine createLine(Location location) {
        return new HologramLine_v1_16_R2(location);
    }

    public void registerCustomArmorStandEntity() {
        Map<String, Type<?>> types = (Map<String, Type<?>>) DataConverterRegistry.a().getSchema(DataFixUtils.makeKey(SharedConstants.getGameVersion().getWorldVersion())).findChoiceType(DataConverterTypes.ENTITY).types();
        types.put("minecraft:hologram_api", types.get("minecraft:armor_stand"));
        EntityTypes.Builder.a(HologramLine_v1_16_R2::new, EnumCreatureType.MISC).a("hologram_api");
    }
}
