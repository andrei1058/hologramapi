package com.andrei1058.hologramapi;

import net.minecraft.server.v1_10_R1.EntityTypes;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Map;

public class HologramAdapter_v1_10_R1 implements HologramVersion {

    public void registerPlayer(Player player) {
        if (((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().get("HologramAPI") == null) {
            ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().addBefore("packet_handler", " HologramAPI", new PacketListener_v1_10_R1(player.getUniqueId()));
        }
    }

    public EntityHologramLine createLine(Location location) {
        return new HologramLine_v1_10_R1(location);
    }

    @SuppressWarnings("rawtypes")
    public void registerCustomArmorStandEntity() {
        try {
            ArrayList<Map> dataMap = new ArrayList<>();
            for (Field f : EntityTypes.class.getDeclaredFields()) {
                if (!f.getType().getSimpleName().equals(Map.class.getSimpleName())) continue;
                f.setAccessible(true);
                dataMap.add((Map) f.get(null));
            }
            if (dataMap.get(2).containsKey(30)) {
                dataMap.get(0).remove("EntityHologram");
                dataMap.get(2).remove(30);
            }
            Method method = EntityTypes.class.getDeclaredMethod("a", Class.class, String.class, Integer.TYPE);
            method.setAccessible(true);
            method.invoke(null, HologramLine_v1_10_R1.class, "EntityHologram", 30);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
