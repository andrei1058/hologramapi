[![Discord Server](https://discord.com/api/guilds/201345265821679617/widget.png)](https://discord.gg/XdJfN2X)

### Supported MC versions
- 1.8 r3
- 1.9 r2
- 1.10 r1
- 1.11 r1
- 1.12 r1
- 1.13 r2
- 1.14 r1
- 1.15 r1
- 1.16 r1, r2, r3
- 1.20 r2

### Creating holograms
```java
public final class HoloExample extends JavaPlugin {

    private static HologramAPI hologramAPI;

    @Override
    public void onEnable() {
        try {
            hologramAPI = new HologramAPI(this);
        } catch (InstantiationException e) {
            e.printStackTrace();
            // SERVER NOT SUPPORTED
        }
    }

    public static void createHologram(){
        
        Hologram hologram = new Hologram(location, linesNumber);
        
        // the first page is created automatically
        HologramPage page1 = hologram.getPage(0);
        // setting first line content
        page1.setLineContent(0, new LineTextContent((player) -> "&6Hello " + player.getName()));
        // setting second line content (a refreshable line)
        AtomicInteger sec = new AtomicInteger();
        page1.setLineContent(1, new LineTextContent((player) -> "&3Counter " + sec.getAndIncrement()));
        Bukkit.getScheduler().runTaskTimer(this, () -> {
            hologram.refreshLine(1);
        }, 1L, 20L);
        // setting third line content
        hologramPage.setLineContent(2, new LineTextContent(player -> "&6Just another line"));

        // creating new pages
        HologramPage page2 = hologram.createNewPage();

        // setting the hologram click listener (example of a page switcher)
        hologram.setClickListener((player, clickType) -> {
            int currentPage = hologram.getPlayerCurrentPage(player.getUniqueId());
            if (clickType == HologramClickListener.LineClickType.LEFT_CLICK) {
                hologram.setPlayerCurrentPage(player, currentPage + 1);
            } else {
                hologram.setPlayerCurrentPage(player, currentPage - 1);
            }
        });

        // disabling hologram collisions (pvp, arrows, etc.)
        // this will disable the click listener
        hologram.allowCollisions(false);
    
        // hide the hologram for all players
        hologram.hide();
    
        // hide the hologram for target player
        hologram.hide(player);

        // show the hologram previously hidden with #hide()
        // this will not affect hidden holograms per player, aka #hide(player)
        hologram.show();

        // show a hologram previously hidden with #hide(player)
        hologram.show(player);
        
        // check if a player can see the hologram
        // works only if it was hidden with #hide(player)
        hologram.isHiddenFor(player);
    
        // add a top margin to a hologram line (spacing)
        hologram.setMarginTop(lineNumber, 0.4);

        hologram.remove();
    }
}
```

### Maven
```xml
<repositories>
    <repository>
        <id>codemc-repo</id>
        <url>https://repo.codemc.io/repository/maven-public/</url>
    </repository>
</repositories>
```
```xml
<dependencies>
    <dependency>
        <groupId>com.andrei1058.hologramapi</groupId>
        <artifactId>hologram-api</artifactId>
        <version>{version}</version>
        <scope>compile</scope>
    </dependency>
</dependencies>
```