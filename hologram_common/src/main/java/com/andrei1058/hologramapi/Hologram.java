package com.andrei1058.hologramapi;

import com.andrei1058.hologramapi.content.HologramClickListener;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@SuppressWarnings("unused")
@ApiStatus.Experimental
public class Hologram {

    private final Map<UUID, Integer> playerCurrentPage = new WeakHashMap<>();
    private final LinkedList<HologramPage> hologramPages = new LinkedList<>();
    private final LinkedList<EntityHologramLine> lines = new LinkedList<>();
    private final List<UUID> hiddenForPlayer = new ArrayList<>();

    private boolean collisionsRule = true;

    private HologramClickListener clickListener = null;
    private static final double DISTANCE = 0.3;

    public Hologram(Location location, int lines) {
        for (int i = 0; i < lines; i++) {
            EntityHologramLine entityHologramLine = HologramAPI.hologramVersion.createLine(location);
            entityHologramLine.setMarginTop(DISTANCE);
            this.lines.add(entityHologramLine);
            HologramAPI.hologramByEntityId.put(entityHologramLine.getId(), this);
            location.subtract(0, entityHologramLine.getMarginTop(), 0);
        }
        createNewPage();
        this.lines.forEach(EntityHologramLine::refresh);
    }

    /**
     * Get current page of a player.
     */
    public int getPlayerCurrentPage(UUID player) {
        return playerCurrentPage.getOrDefault(player, 0);
    }

    /**
     * Show a page.
     *
     * @param player target player.
     * @param page   page to show.
     */
    public void setPlayerCurrentPage(Player player, int page) {
        if (page < 0) return;
        if (playerCurrentPage.getOrDefault(player.getUniqueId(), 0) == page) return;
        if (page == 0) {
            playerCurrentPage.remove(player.getUniqueId());
            refreshLines(player);
            return;
        }
        if (page >= hologramPages.size()) return;
        if (playerCurrentPage.containsKey(player.getUniqueId())) {
            playerCurrentPage.replace(player.getUniqueId(), page);
            return;
        }
        playerCurrentPage.put(player.getUniqueId(), page);
        refreshLines(player);
    }

    @SuppressWarnings("UnusedReturnValue")
    public HologramPage createNewPage() {
        HologramPage page = new HologramPage(lines);
        hologramPages.add(page);
        return page;
    }

    /**
     * Get page by number. Starting with 0.
     */
    @Nullable
    public HologramPage getPage(int page) {
        if (hologramPages.size() <= page) return null;
        return hologramPages.get(page);
    }

    public void refreshLine(int line) {
        if (lines.size() <= line) return;
        EntityHologramLine l = lines.get(line);
        if (l == null) return;
        l.refresh();
    }

    public void refreshLine(int line, Player player) {
        if (lines.size() <= line) return;
        EntityHologramLine l = lines.get(line);
        if (l == null) return;
        l.refresh(player);
    }

    public void refreshLines() {
        for (EntityHologramLine line : lines){
            line.refresh();
        }
    }

    public void refreshLines(Player player) {
        for (EntityHologramLine line : lines){
            line.refresh(player);
        }
    }

    public void setClickListener(HologramClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public HologramClickListener getClickListener() {
        return clickListener;
    }

    public void allowCollisions(boolean toggle) {
        collisionsRule = toggle;
        for (EntityHologramLine line : lines){
            line.allowCollisions(toggle);
        }
    }

    public void hide() {
        for (EntityHologramLine line : lines){
            line.hide();
        }
    }

    public void hide(Player player) {
        if (!hiddenForPlayer.contains(player.getUniqueId())) {
            hiddenForPlayer.add(player.getUniqueId());
            for (EntityHologramLine line : lines){
                line.hide(player);
            }
        }
    }

    /**
     * Will not affect users that have applied {@link #hide(Player)}.
     */
    public void show() {
        for (EntityHologramLine line : lines) {
            line.show();
            line.allowCollisions(collisionsRule);
        }
    }

    /**
     * Won't work if {@link #hide()} was applied.
     */
    public void show(Player player) {
        hiddenForPlayer.remove(player.getUniqueId());
        for (EntityHologramLine line : lines) {
            line.show(player);
        }
    }

    public boolean isHiddenFor(Player player) {
        return hiddenForPlayer.contains(player.getUniqueId());
    }

    public boolean isHiddenFor(UUID player) {
        return hiddenForPlayer.contains(player);
    }

    protected void removeFromHiddenList(UUID uuid) {
        hiddenForPlayer.remove(uuid);
    }

    /**
     * Change top margin. First line is 0.
     */
    public void setMarginTop(int line, double marginTop) {
        if (line < 0) return;
        if (line >= lines.size()) return;
        EntityHologramLine entityHologramLine2 = lines.get(line);
        entityHologramLine2.setMarginTop(marginTop + entityHologramLine2.getMarginTop());
        for (int i = line + 1; i < lines.size(); i++) {
            EntityHologramLine entityHologramLine = lines.get(i);
            entityHologramLine.setMarginTop(marginTop + entityHologramLine.getMarginTop());
        }
    }

    public double getMarginTop(int line) {
        if (line < 0) return 0;
        if (line >= lines.size()) return 0;
        return lines.get(line).getMarginTop();
    }

    public void remove() {
        for (EntityHologramLine line : lines) {
            line.remove();
        }
        playerCurrentPage.clear();
        hologramPages.clear();
        hiddenForPlayer.clear();
    }

    /**
     * Get number of lines.
     */
    public int getLines() {
        return lines.size();
    }
}
