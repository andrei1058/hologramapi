package com.andrei1058.hologramapi;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.ApiStatus;

@ApiStatus.Experimental
interface HologramVersion {

    /**
     * This must be used when a player joins the server
     * in order to modify armor stand packets before sending them.
     */
    void registerPlayer(Player player);

    /**
     * Create a new line for current version.
     */
    EntityHologramLine createLine(Location location);

    /**
     * Register custom armor stand entity.
     */
    void registerCustomArmorStandEntity();
}
