package com.andrei1058.hologramapi;

import com.andrei1058.hologramapi.content.HologramClickListener;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.jetbrains.annotations.ApiStatus;

@ApiStatus.Experimental
public class ArmorStandInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerArmorStandManipulateEvent e) {
        if (e.isCancelled()) return;
        if (e.getRightClicked() == null) return;
        if (HologramAPI.isHologram(e.getRightClicked())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    // right click
    public void onInteract2(PlayerInteractAtEntityEvent e) {
        if (e.getRightClicked() == null) return;
        if (e.getRightClicked().getType() != EntityType.ARMOR_STAND) return;

        if (!HologramAPI.isHologram((ArmorStand) e.getRightClicked())) return;
        e.setCancelled(true);

        Hologram hologram = HologramAPI.getHologramByEntityId(e.getRightClicked().getEntityId());
        if (hologram != null) {
            HologramClickListener listener = hologram.getClickListener();
            if (listener != null) {
                listener.onClick(e.getPlayer(), HologramClickListener.LineClickType.RIGHT_CLICK);
            }
        }

    }

    @EventHandler
    // left click
    public void onInteract3(EntityDamageByEntityEvent e) {
        if (e.getEntity() == null) return;
        if (e.getDamager() == null) return;
        if (e.getDamager().getType() != EntityType.PLAYER) return;
        if (e.getEntity().getType() != EntityType.ARMOR_STAND) return;

        if (!HologramAPI.isHologram((ArmorStand) e.getEntity())) return;
        e.setCancelled(true);

        Hologram hologram = HologramAPI.getHologramByEntityId(e.getEntity().getEntityId());
        if (hologram != null) {
            HologramClickListener listener = hologram.getClickListener();
            if (listener != null) {
                listener.onClick((Player) e.getDamager(), HologramClickListener.LineClickType.LEFT_CLICK);
            }
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        HologramAPI.registerPlayer(e.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        HologramAPI.onDisconnect(e.getPlayer().getUniqueId());
    }
}
