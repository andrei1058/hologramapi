package com.andrei1058.hologramapi;

import com.andrei1058.hologramapi.content.LineContent;
import org.bukkit.Bukkit;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.UUID;

@ApiStatus.Experimental
public class HologramAPI {

    protected static HashMap<Integer, Hologram> hologramByEntityId = new HashMap<>();

    protected static HologramVersion hologramVersion;
    protected static ArmorStandInteractListener armorStandInteractListener;

    /**
     * Packet channel name.
     */
    public static final String CHANNEL_NAME = "dev_andrei1058_hologram";

    public HologramAPI(Plugin plugin) throws InstantiationException {
        try {
            Class<?> c = Class.forName("com.andrei1058.hologramapi.HologramAdapter_" + Bukkit.getServer().getClass().getName().split("\\.")[3]);
            hologramVersion = (HologramVersion) c.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException e) {
            throw new InstantiationException("Server not supported.");
        }
        if (armorStandInteractListener == null){
            armorStandInteractListener = new ArmorStandInteractListener();
            Bukkit.getPluginManager().registerEvents(armorStandInteractListener, plugin);
        }
    }

    /**
     * This must be used when a player joins the server
     * in order to modify armor stand packets before sending them.
     */
    protected static void registerPlayer(Player player) {
        hologramVersion.registerPlayer(player);
    }

    /**
     * Get hologram entity content by armor stand entity ID.
     *
     * @param entityId NMS entity ID.
     * @param player   target player.
     */
    protected static @Nullable LineContent getContentForEntityId(int entityId, UUID player) {
        Hologram hologram = hologramByEntityId.get(entityId);
        if (hologram == null) return null;

        HologramPage hologramPage = hologram.getPage(hologram.getPlayerCurrentPage(player));
        if (hologramPage == null) return null;

        return hologramPage.getLineByEntityId(entityId);
    }

    protected static boolean isHologram(ArmorStand armorStand){
        return hologramByEntityId.get(armorStand.getEntityId()) != null;
    }

    public static @Nullable Hologram getHologramByEntityId(int entityId) {
        return hologramByEntityId.get(entityId);
    }

    /**
     * This is used to remove user data when he leaves the server.
     */
    protected static void onDisconnect(UUID player) {
        hologramByEntityId.values().parallelStream().forEach(h -> h.removeFromHiddenList(player));
    }
}
