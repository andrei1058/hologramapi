package com.andrei1058.hologramapi;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.ApiStatus;

@ApiStatus.Experimental
interface EntityHologramLine {

    /**
     * Get the entity id.
     */
    int getId();

    /**
     * Refresh line content for all players.
     */
    void refresh();

    double getMarginTop();

    void setMarginTop(double marginTop);

    /**
     * This will enable/ disable the interact listener.
     */
    void allowCollisions(boolean toggle);

    void refresh(Player player);

    void hide();

    void hide(Player player);

    void show();

    void show(Player player);

    void remove();
}
