package com.andrei1058.hologramapi;

import com.andrei1058.hologramapi.content.LineContent;
import com.andrei1058.hologramapi.content.LineTextContent;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

@ApiStatus.Experimental
public class HologramPage {

    private final LinkedHashMap<Integer, LineContent> lineByEntityId = new LinkedHashMap<>();

    HologramPage(@NotNull LinkedList<EntityHologramLine> entities) {
        entities.forEach(entity -> lineByEntityId.put(entity.getId(), new LineTextContent()));
    }

    /**
     * Get page lines.
     */
    public Collection<LineContent> getLines() {
        return lineByEntityId.values();
    }

    public void setLineContent(int line, LineContent content){
        if (line < 0) return;
        if (line >= lineByEntityId.size()) return;

        int current = 0;
        for (Map.Entry<Integer, LineContent> entry : lineByEntityId.entrySet()){
            if (line == current){
                entry.setValue(content);
                break;
            }
            current++;
        }
    }

    /**
     * Get line content by NMS entity Id.
     *
     * @param entityId NMS entity id.
     */
    @Nullable
    public LineContent getLineByEntityId(int entityId) {
        return lineByEntityId.get(entityId);
    }
}
