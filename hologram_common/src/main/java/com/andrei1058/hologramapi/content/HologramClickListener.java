package com.andrei1058.hologramapi.content;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.ApiStatus;

@ApiStatus.Experimental
public interface HologramClickListener {

    void onClick(Player player, LineClickType lineClickType);

    enum LineClickType {
        LEFT_CLICK, RIGHT_CLICK
    }
}
