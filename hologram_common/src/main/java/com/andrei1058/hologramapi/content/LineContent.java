package com.andrei1058.hologramapi.content;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

@ApiStatus.Experimental
public interface LineContent {

    /**
     * Get content for target player.
     */
    @NotNull
    Function<Player, ?> getContent();
}
