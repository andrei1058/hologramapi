package com.andrei1058.hologramapi.content;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

@ApiStatus.Experimental
public class LineItemContent implements LineContent {

    private Function<Player, ItemStack> content;

    public LineItemContent() {
        content = s -> new ItemStack(Material.DIAMOND);
        throw new UnsupportedOperationException("Not implemented yet!");
    }

    public LineItemContent(Function<Player, ItemStack> content) {
        this.content = content;
        throw new UnsupportedOperationException("Not implemented yet!");
    }

    public @NotNull Function<Player, ItemStack> getContent() {
        return content;
    }

    public void setContent(Function<Player, ItemStack> content) {
        this.content = content;
    }
}
