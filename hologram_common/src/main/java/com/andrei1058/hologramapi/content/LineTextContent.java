package com.andrei1058.hologramapi.content;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

@ApiStatus.Experimental
public class LineTextContent implements LineContent {

    private Function<Player, String> content;

    public LineTextContent(){
        content = s -> " ";
    }

    public LineTextContent(Function<Player, String> content){
        this.content = content;
    }

    public @NotNull Function<Player, String> getContent() {
        return content;
    }

    public void setContent(Function<Player, String> content) {
        this.content = content;
    }
}
