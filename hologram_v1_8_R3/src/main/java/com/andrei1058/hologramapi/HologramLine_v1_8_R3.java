package com.andrei1058.hologramapi;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.jetbrains.annotations.NotNull;

public class HologramLine_v1_8_R3 extends EntityArmorStand implements EntityHologramLine {

    private double marginTop = 0;
    private final double originalY;

    public HologramLine_v1_8_R3(@NotNull Location location) {
        super(((CraftWorld) location.getWorld()).getHandle());
        setInvisible(true);
        setCustomNameVisible(true);
        setCustomName("Hologram: " + getId());
        setGravity(true);
        setArms(false);
        setBasePlate(false);
        setSmall(true);
        this.originalY = location.getY();
        setLocation(location.getX(), location.getY() - getMarginTop(), location.getZ(), location.getYaw(), location.getPitch());
        ((CraftLivingEntity) this.getBukkitEntity()).setRemoveWhenFarAway(false);
        ((CraftWorld) location.getWorld()).getHandle().addEntity(this, CreatureSpawnEvent.SpawnReason.CUSTOM);
    }

    public void refresh() {
        setCustomName("Hologram: " + getId());
    }

    @Override
    public double getMarginTop() {
        return marginTop;
    }

    @Override
    public void setMarginTop(double marginTop) {
        this.marginTop = marginTop;
        this.bukkitEntity.teleport(new Location(world.getWorld(), this.locX, this.originalY - this.marginTop, this.locZ), PlayerTeleportEvent.TeleportCause.PLUGIN);
    }

    @Override
    public void allowCollisions(boolean toggle) {
        ((ArmorStand) bukkitEntity).setMarker(!toggle);
    }

    @Override
    public void refresh(Player player) {
        sendPacketToPlayer(player, new PacketPlayOutEntityMetadata(getId(), getDataWatcher(), true));
    }

    @Override
    public void hide() {
        setCustomNameVisible(false);
        allowCollisions(false);
    }

    @Override
    public void hide(Player player) {
        sendPacketToPlayer(player, new PacketPlayOutEntityDestroy(this.getId()));
    }

    @Override
    public void show() {
        setCustomNameVisible(true);
    }

    @Override
    public void show(Player player) {
        sendPacketToPlayer(player, new PacketPlayOutSpawnEntity (this, 78));
        sendPacketToPlayer(player, new PacketPlayOutEntityMetadata (this.getId(), getDataWatcher(), true));
    }

    @Override
    public void remove() {
        getBukkitEntity().remove();
    }

    @Override
    public void die() {
    }

    @Override
    public void die(DamageSource damagesource) {
    }

    @Override
    protected void damageArmor(float f) {
    }

    @Override
    public boolean damageEntity(DamageSource damagesource, float f) {
        return false;
    }

    private static void sendPacketToPlayer(@NotNull Player player, @NotNull Packet<?> packet) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }
}
