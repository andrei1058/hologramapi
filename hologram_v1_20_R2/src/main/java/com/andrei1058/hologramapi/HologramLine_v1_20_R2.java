package com.andrei1058.hologramapi;

import net.minecraft.network.chat.IChatBaseComponent;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.PacketPlayOutEntityDestroy;
import net.minecraft.network.protocol.game.PacketPlayOutEntityMetadata;
import net.minecraft.network.protocol.game.PacketPlayOutSpawnEntity;
import net.minecraft.sounds.SoundEffect;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityTypes;
import net.minecraft.world.entity.decoration.EntityArmorStand;
import net.minecraft.world.level.World;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_20_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_20_R2.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_20_R2.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class HologramLine_v1_20_R2 extends EntityArmorStand implements EntityHologramLine {

    private double marginTop = 0;
    private double originalY;

    // name toggle for sending refresh packet
    private boolean nameA = false;
    private static String name1 = "NAME1";
    private static String name2 = "NAME2";

    public HologramLine_v1_20_R2(@NotNull Location location) {
        super(((CraftWorld) Objects.requireNonNull(location.getWorld())).getHandle(), location.getX(), location.getY(), location.getZ());
        // set invisible
        j(true);
        // set custom name visible
        n(true);
        this.getBukkitEntity().setCustomName(name2);
        // no gravity
        e(true);
        // todo setArms(false);
        // set base-plate
        s(false);
        // set small
        t(true);
        this.originalY = location.getY();
        // ticksFarFromPlayer
        bf = 0;
        //no clip
        Q = true;
        // set silent
        d(true);
        // set location
        a(location.getX(), location.getY() - getMarginTop(), location.getZ(), location.getYaw(), location.getPitch());
        // do not remove when far away
        ((CraftLivingEntity) this.getBukkitEntity()).setRemoveWhenFarAway(false);
        ((CraftWorld) location.getWorld()).getHandle().addFreshEntity(this, CreatureSpawnEvent.SpawnReason.CUSTOM);
    }

    public HologramLine_v1_20_R2(EntityTypes<? extends EntityArmorStand> entitytypes, World world) {
        super(entitytypes, world);
    }

    @Override
    public int getId() {
        return this.ah();
    }

    public void refresh() {
        getBukkitEntity().setCustomName(nameA ? name2 : name1);
        nameA = !nameA;
    }

    @Override
    public double getMarginTop() {
        return marginTop;
    }

    @Override
    public void setMarginTop(double marginTop) {
        this.marginTop = marginTop;
        this.getBukkitEntity().teleport(new Location(
                this.dL().getWorld(),
                this.dj().c,
                this.originalY - this.marginTop,
                this.dj().e
        ), PlayerTeleportEvent.TeleportCause.PLUGIN);
    }

    @Override
    public void allowCollisions(boolean toggle) {
        ((ArmorStand) getBukkitEntity()).setMarker(!toggle);
    }

    @Override
    public boolean canCollideWithBukkit(Entity entity) {
        return false;
    }

    @Override
    public void refresh(Player player) {
        sendPacketToPlayer(player, new PacketPlayOutEntityMetadata(getId(), this.al().c()));
    }

    @Override
    public void hide() {
        // hide display name
        n(false);
        // disable collisions
        allowCollisions(false);
    }

    @Override
    public void hide(Player player) {
        sendPacketToPlayer(player, new PacketPlayOutEntityDestroy(this.getId()));
    }

    @Override
    public void show() {
        // show display name
        n(false);
    }

    @Override
    public void show(Player player) {
        sendPacketToPlayer(player, new PacketPlayOutSpawnEntity(this, 78));
        sendPacketToPlayer(player, new PacketPlayOutEntityMetadata(getId(), this.al().c()));
    }

    @Override
    public void remove() {
        getBukkitEntity().remove();
    }

    @Override
    public void inactiveTick() {
    }

    @Override
    public void aq() {
        // disable base tick
    }

    @Override
    public void l() {
        // entity living tick

        // todo experimental
        // this triggers a metadata packet for players in range, handled by nms
        // in the future needs to be replaced with more detailed packet handling to reduce packet spam
        // note1: this runs on main thread... to find a way to send packets async
        refresh();
    }

    @Override
    public void a(DamageSource damagesource){

    }

    public void a(RemovalReason entity_removalreason) {

    }

    private static void sendPacketToPlayer(@NotNull Player player, @NotNull Packet<?> packet) {
        ((CraftPlayer) player).getHandle().c.b(packet);
    }

    @Override
    protected boolean damageEntity0(DamageSource damagesource, float f) {
        return false;
    }

    public boolean a(DamageSource damagesource, float f){
        return false;
    }

    public boolean b(DamageSource damagesource) {
        return false;
    }

    @Contract(pure = true)
    protected @Nullable SoundEffect aM() {
        return null;
    }

    @Contract(pure = true)
    @Override
    protected @Nullable SoundEffect aN() {
        return null;
    }

    @Override
    protected @Nullable SoundEffect aL() {
        return null;
    }

    @Contract(pure = true)
    @Override
    public @Nullable SoundEffect getSwimSound0() {
        return null;
    }

    @Contract(pure = true)
    @Override
    public @Nullable SoundEffect getSwimSplashSound0() {
        return null;
    }

    @Contract(pure = true)
    @Override
    public @Nullable SoundEffect getSwimHighSpeedSplashSound0() {
        return null;
    }

    @Contract(pure = true)
    @Override
    public @Nullable SoundEffect getHurtSound0(DamageSource damagesource) {
        return null;
    }

    @Contract(pure = true)
    @Override
    public @Nullable SoundEffect getDeathSound0() {
        return null;
    }
}
