package com.andrei1058.hologramapi;

import net.minecraft.network.NetworkManager;
import net.minecraft.server.network.PlayerConnection;
import net.minecraft.server.network.ServerCommonPacketListenerImpl;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_20_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

public class HologramAdapter_v1_20_R2 implements HologramVersion {

    private Field networkManagerField;

    private void reflectNetworkManagerField() {
        if (networkManagerField == null) {
            try {
                Field networkManagerField = ServerCommonPacketListenerImpl.class.getDeclaredField("c");
                networkManagerField.setAccessible(true);
                this.networkManagerField = networkManagerField;
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void registerPlayer(Player player) {

        reflectNetworkManagerField();

        if (null == networkManagerField) {
            return;
        }


        try {

            PlayerConnection playerConnection = ((CraftPlayer)player).getHandle().c;
            NetworkManager networkManager = (NetworkManager) networkManagerField.get(playerConnection);

            if (networkManager.n.pipeline().get(HologramAPI.CHANNEL_NAME) == null){
                networkManager.n.pipeline().addBefore(
                        "packet_handler",
                        HologramAPI.CHANNEL_NAME,
                        new PacketListener_v1_20_R2(player.getUniqueId())
                );
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public EntityHologramLine createLine(Location location) {
        return new HologramLine_v1_20_R2(location);
    }

    public void registerCustomArmorStandEntity() {
//        Map<String, Type<?>> types = (Map<String, Type<?>>) DataConverterRegistry.a().getSchema(DataFixUtils.makeKey(SharedConstants.getGameVersion().getWorldVersion())).findChoiceType(DataConverterTypes.ENTITY).types();
//        types.put("minecraft:hologram_api", types.get("minecraft:armor_stand"));
//        EntityTypes.Builder.a(HologramLine_v1_20_R2::new, EnumCreatureType.MISC).a("hologram_api");
    }
}
