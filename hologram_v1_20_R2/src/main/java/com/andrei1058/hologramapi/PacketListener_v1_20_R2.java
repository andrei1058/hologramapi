package com.andrei1058.hologramapi;

import com.andrei1058.hologramapi.content.LineContent;
import com.andrei1058.hologramapi.content.LineTextContent;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import net.minecraft.network.chat.IChatBaseComponent;
import net.minecraft.network.chat.IChatMutableComponent;
import net.minecraft.network.protocol.game.*;
import net.minecraft.network.syncher.DataWatcher;
import net.minecraft.network.syncher.DataWatcherObject;
import net.minecraft.network.syncher.DataWatcherSerializer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.*;

public class PacketListener_v1_20_R2 extends ChannelDuplexHandler {

    private final UUID player;

    public PacketListener_v1_20_R2(UUID player) {
        this.player = player;
    }

    @Override
    public void write(ChannelHandlerContext channelHandlerContext, Object o, ChannelPromise channelPromise) throws Exception {

        if (o instanceof PacketPlayOutEntityMetadata) {


            int entityId = ((PacketPlayOutEntityMetadata) o).a();
            LineContent lineContent = HologramAPI.getContentForEntityId(entityId, player);

            if (lineContent != null) {
                if (lineContent instanceof LineTextContent) {
                    String line = ChatColor.translateAlternateColorCodes('&', (String) lineContent.getContent().apply(Bukkit.getPlayer(player)));


                    List<DataWatcher.b<?>> remapped = new ArrayList<>();

                    for (DataWatcher.b<?> item : ((PacketPlayOutEntityMetadata) o).d()) {
                        if (item.a() == 2) {
                            DataWatcherSerializer<Optional<IChatMutableComponent>> serializer = (DataWatcherSerializer<Optional<IChatMutableComponent>>) item.b();
                            DataWatcher.b<?> item4 = DataWatcher.b.a(
                                    new DataWatcherObject<>(2, serializer),
                                    Optional.ofNullable(IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + line + "\"}"))
                            );
                            remapped.add(item4);

                        } else {
                            remapped.add(item);
                        }
                    }
                    o = new PacketPlayOutEntityMetadata(entityId, remapped);
                }
                /* else if (lineContent instanceof LineItemContent) {
                    Player p = Bukkit.getPlayer(player);

                    EntityItem entityItem = new EntityItem(((CraftWorld) p.getWorld()).getHandle(), p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ(), CraftItemStack.asNMSCopy((ItemStack) lineContent.getContent().apply(p)));

                    PlayerConnection playerConnection = ((CraftPlayer) Bukkit.getPlayer(player)).getHandle().playerConnection;

                    PacketPlayOutSpawnEntity spawnEntity = new PacketPlayOutSpawnEntity(entityItem, 2);
                    playerConnection.sendPacket(spawnEntity);

                    PacketPlayOutEntityMetadata entityMetadata = new PacketPlayOutEntityMetadata(entityItem.getId(), entityItem.getDataWatcher(), true);
                    playerConnection.sendPacket(entityMetadata);*/

                    /*for (DataWatcher.WatchableObject wo : (List<DataWatcher.WatchableObject>) getFieldValue(o, "b")) {
                        Bukkit.broadcastMessage(wo.a() + " - a");
                        Bukkit.broadcastMessage(wo.b().getClass().getSimpleName() + " - b");
                        Bukkit.broadcastMessage(wo.c() + " - c");
                        Bukkit.broadcastMessage(wo.d() + " - d");
                    }
                }*/
            }
        } else {
            if (o instanceof PacketPlayOutSpawnEntity) {

                int entityId = ((PacketPlayOutSpawnEntity) o).a();

                Hologram hologram = HologramAPI.getHologramByEntityId(entityId);

                // don't send the packet if the hologram is hidden for this player
                if (hologram != null) {
                    if (hologram.isHiddenFor(player)) {
                        return;
                    }
                }
            }
//            else if (o instanceof PacketPlayOutEntityStatus) {
//                int entityId = ((PacketPlayOutEntityStatus) o).a();
//                Bukkit.broadcastMessage("PacketPlayOutEntityStatus: " + entityId);
//            } else if (o instanceof PacketPlayOutTileEntityData) {
//                Bukkit.broadcastMessage("PacketPlayOutTileEntityData: " + ((PacketPlayOutTileEntityData) o).d().toString());
//            } else if (o instanceof PacketPlayOutEntityTeleport) {
//                int entityId = ((PacketPlayOutEntityTeleport) o).a();
//
////                Hologram hologram = HologramAPI.getHologramByEntityId(entityId);
////                Bukkit.broadcastMessage("PacketPlayOutEntityTeleport: "+entityId);
//            } else if(o instanceof PacketPlayOutEntity.PacketPlayOutRelEntityMove) {
//                int entityId = ((PacketPlayOutEntity.PacketPlayOutRelEntityMove) o).a();
//
//                Hologram hologram = HologramAPI.getHologramByEntityId(entityId);
//                if (null != hologram) {
//                    Bukkit.broadcastMessage("hologram moved: " + entityId);
//                }
//            } else if (o instanceof PacketPlayOutEntityEquipment) {
//                int entityId = ((PacketPlayOutEntityEquipment) o).a();
//
//                Hologram hologram = HologramAPI.getHologramByEntityId(entityId);
//                if (null != hologram) {
//                    Bukkit.broadcastMessage("hologram equipped NOT: " + entityId);
//                    return;
//                }
//            } else {
////                if (o.getClass().getName().toLowerCase().contains("entity")) {
////                    Bukkit.broadcastMessage(o.toString());
////                }
//            }
        }

        super.write(channelHandlerContext, o, channelPromise);
    }
}
