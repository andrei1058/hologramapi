package com.andrei1058.hologramapi;

import net.minecraft.server.v1_12_R1.EntityTypes;
import net.minecraft.server.v1_12_R1.MinecraftKey;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class HologramAdapter_v1_12_R1 implements HologramVersion {

    public void registerPlayer(Player player) {
        if (((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().get("HologramAPI") == null) {
            ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().addBefore("packet_handler", " HologramAPI", new PacketListener_v1_12_R1(player.getUniqueId()));
        }
    }

    public EntityHologramLine createLine(Location location) {
        return new HologramLine_v1_12_R1(location);
    }

    public void registerCustomArmorStandEntity() {
        EntityTypes.b.a(78, new MinecraftKey("EntityHologram"), HologramLine_v1_12_R1.class);
    }
}
