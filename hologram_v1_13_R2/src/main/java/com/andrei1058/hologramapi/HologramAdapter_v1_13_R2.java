package com.andrei1058.hologramapi;

import com.mojang.datafixers.types.Type;
import net.minecraft.server.v1_13_R2.DataConverterRegistry;
import net.minecraft.server.v1_13_R2.DataConverterTypes;
import net.minecraft.server.v1_13_R2.EntityTypes;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.Map;

public class HologramAdapter_v1_13_R2 implements HologramVersion {

    public void registerPlayer(Player player) {
        if (((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().get("HologramAPI") == null) {
            ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().addBefore("packet_handler", " HologramAPI", new PacketListener_v1_13_R2(player.getUniqueId()));
        }
    }

    public EntityHologramLine createLine(Location location) {
        return new HologramLine_v1_13_R2(location);
    }

    public void registerCustomArmorStandEntity() {
        Map<Object, Type<?>> types = (Map<Object, Type<?>>) DataConverterRegistry.a().getSchema(15190).findChoiceType(DataConverterTypes.n).types();
        types.put("minecraft:hologram_api", types.get("minecraft:armor_stand"));
        EntityTypes.a("hologram_api", EntityTypes.a.a(HologramLine_v1_13_R2.class));
    }
}
