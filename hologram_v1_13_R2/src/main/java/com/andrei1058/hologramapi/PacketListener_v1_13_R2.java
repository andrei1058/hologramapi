package com.andrei1058.hologramapi;

import com.andrei1058.hologramapi.content.LineContent;
import com.andrei1058.hologramapi.content.LineTextContent;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import net.minecraft.server.v1_13_R2.*;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntityMetadata;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class PacketListener_v1_13_R2 extends ChannelDuplexHandler {

    private final UUID player;

    public PacketListener_v1_13_R2(UUID player) {
        this.player = player;
    }

    @Override
    public void write(ChannelHandlerContext channelHandlerContext, Object o, ChannelPromise channelPromise) throws Exception {
        if (o instanceof PacketPlayOutEntityMetadata) {

            int entityId = getFieldValue(o, "a");

            LineContent lineContent = HologramAPI.getContentForEntityId(entityId, player);

            if (lineContent != null) {
                if (lineContent instanceof LineTextContent) {
                    String line = ChatColor.translateAlternateColorCodes('&', (String) lineContent.getContent().apply(Bukkit.getPlayer(player)));

                    //noinspection unchecked,rawtypes,rawtypes
                    for (DataWatcher.Item wo : (List<DataWatcher.Item>) getFieldValue(o, "b")) {
                        // custom name
                        if (wo.a().a() == 2) {
                            //noinspection unchecked
                            wo.a(Optional.of(Objects.requireNonNull(IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + line + "\"}"))));
                            break;
                        }
                    }
                }
                /* else if (lineContent instanceof LineItemContent) {
                    Player p = Bukkit.getPlayer(player);

                    EntityItem entityItem = new EntityItem(((CraftWorld) p.getWorld()).getHandle(), p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ(), CraftItemStack.asNMSCopy((ItemStack) lineContent.getContent().apply(p)));

                    PlayerConnection playerConnection = ((CraftPlayer) Bukkit.getPlayer(player)).getHandle().playerConnection;

                    PacketPlayOutSpawnEntity spawnEntity = new PacketPlayOutSpawnEntity(entityItem, 2);
                    playerConnection.sendPacket(spawnEntity);

                    PacketPlayOutEntityMetadata entityMetadata = new PacketPlayOutEntityMetadata(entityItem.getId(), entityItem.getDataWatcher(), true);
                    playerConnection.sendPacket(entityMetadata);*/

                    /*for (DataWatcher.WatchableObject wo : (List<DataWatcher.WatchableObject>) getFieldValue(o, "b")) {
                        Bukkit.broadcastMessage(wo.a() + " - a");
                        Bukkit.broadcastMessage(wo.b().getClass().getSimpleName() + " - b");
                        Bukkit.broadcastMessage(wo.c() + " - c");
                        Bukkit.broadcastMessage(wo.d() + " - d");
                    }
                }*/
            }
        } else {
            if (o instanceof PacketPlayOutSpawnEntity) {

                int entityId = getFieldValue(o, "a");

                Hologram hologram = HologramAPI.getHologramByEntityId(entityId);

                // don't send the packet if the hologram is hidden for this player
                if (hologram != null) {
                    if (hologram.isHiddenFor(player)) return;
                }
            }
        }
        super.write(channelHandlerContext, o, channelPromise);
    }


    @SuppressWarnings("unchecked")
    public static <T> T getFieldValue(@NotNull Object instance, String fieldName) throws Exception {
        Field field = instance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return (T) field.get(instance);
    }
}
